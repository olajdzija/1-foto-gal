
import skyLine from './assets/city-skyline.jpg';
import CNtower from './assets/cn-tower.jpg';
import Rogers from './assets/rogers-center.jpg';
import ClockTower from './assets/old-city-hall.jpg';
import rom from './assets/rom.jpg';
import CityHall from './assets/new-city-hall.jpg';


import './App.css';
import { useState } from 'react';
import React from 'react';


function App() {


  let images = [
    { imgs: skyLine, title:"Toronto",desc:"The Skyline"  },
    { imgs: CNtower, title:"The  CN  tower",desc:"Previously the world's tallest building"  },
    { imgs: Rogers, title:"The  Rogers Center",desc:"Toronto's Iconic Skydome"  },
    { imgs: ClockTower, title:"Old City Hall",desc:"The Clock Tower"  },
    { imgs: rom, title:"ROM",desc:"Canada's most visited museum"  },
    { imgs: CityHall, title:"Toronto  City  Hall",desc:"An example of Brutalist architecture"  },
    
  ];

  const [selectedImage, setSelectedImage] = useState(null);

  return (
    <div className="App">
      <header className="App-header">
        <p>
          The City of Toronto
        </p>
        <p>Historical Sites & More</p>
      </header>

      
      
      <div class="grid">
      {images.map(image => (
       <div class="image_box">
            <div class="overlay-image" >
              <a>
            <img class="image" src={image.imgs} alt="logo" onClick={()=>setSelectedImage(image.imgs)}/>
            
            <div class="shape">
            <h3>{image.title}</h3>
                <br />{image.desc}
                </div>
              </a>
              </div>
              </div>
       
      ))}
      </div>

      <footer>
        
        <ul class="icons">
        
          <li><i class="bi bi-facebook"></i></li>
          <li><i class="bi bi-twitter"></i></li>
          <li><i class="bi bi-instagram"></i></li>
          <li><i class="bi bi-linkedin"></i></li>
        </ul>
        <p>&copy; 2022 York Full Stack</p>
      </footer>
      
      <div id='overlay' style={{visibility: selectedImage ? 'visible': 'hidden'}}>
        <h1><a class="close" onClick={ ()=>setSelectedImage(null) }>X</a></h1>
        <img src={selectedImage} />
      </div>
      </div>
  );
}

export default App;
